package org.cloudbus.cloudsim.examples.power.planetlab;

import org.cloudbus.cloudsim.examples.power.planetlab.EnhancedRunner;

public class SectorAllocationSimpleExample
{
	public static void main(String[] args)
	{
		try {
		String input = new String("C:\\Users\\Admin\\Documents\\cloudsim-3.0.3\\examples\\workload\\LANL\\LANL-CM5-short.swf");
		new EnhancedRunner(input,10,"output",2); // 1 failure zone is full datacenter
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}