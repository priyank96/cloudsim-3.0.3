package org.cloudbus.cloudsim.examples.power.planetlab;


import java.util.Calendar;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSubmitTime;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterBrokerSubmitTime;
import org.cloudbus.cloudsim.EnhancedHost;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Sector;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.EnhancedHelper;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.power.EnhancedPowerDatacenter;
import org.cloudbus.cloudsim.power.HillClimbingAlgorithm;
import org.cloudbus.cloudsim.power.RandomBiasedSampling;
import org.cloudbus.cloudsim.power.ReplicationEnhancedPowerDatacenter;
import org.cloudbus.cloudsim.power.SectorAllocationPolicyHoneyBeeV2;
import org.cloudbus.cloudsim.power.RandomBiasedSampling;
import org.cloudbus.cloudsim.power.SectorAllocationSimple;
import org.cloudbus.cloudsim.util.WorkloadFileReaderLANL;
import org.cloudbus.cloudsim.util.WorkloadFileReaderLANLSubmitTime;


public class EnhancedRunner 
{
	


	/** The broker. */
	protected static DatacenterBrokerSubmitTime broker;

	/** The cloudlet list. */
	protected static List<CloudletSubmitTime> cloudletList;

	/** The vm list. */
	protected static List<Vm> vmList;

	/** The host list. */
	protected static List<EnhancedHost> hostList;
	
	protected static List<Sector> sectorList;
	
	public EnhancedRunner(String inputFile, int hostsPerSector, String outputFolder,int NumOfFailureZones)
	{
		// call init
		init(inputFile, hostsPerSector,NumOfFailureZones); // 1 failure zone by default
		// call start
		start("SimpleSectorAllocation",outputFolder,hostsPerSector,NumOfFailureZones);
	}
	
	protected void init(String inputFile, int hostsPerSector, int numFailureZones) {
		try {
			CloudSim.init(1, Calendar.getInstance(), false);

			broker = (DatacenterBrokerSubmitTime) EnhancedHelper.createBroker();
			int brokerId = broker.getId();
			
			WorkloadFileReaderLANLSubmitTime workloadReader = new WorkloadFileReaderLANLSubmitTime(inputFile, Constants.HOST_MIPS[0]); //1860, rating in MIPS
			cloudletList = workloadReader.generateWorkload(brokerId);
			Log.printLine("brokerId: "+brokerId);
			for(Cloudlet cl : cloudletList)
			{
				cl.setUserId(brokerId);
			}
			vmList = Helper.createVmList(brokerId, cloudletList.size());
			//hostList = EnhancedHelper.createEnhancedHostList(40);
			hostList = EnhancedHelper.createEnhancedHostList(100);
			//sectorList = EnhancedHelper.MakeSectorList(hostList,hostsPerSector);
			sectorList = EnhancedHelper.MakeSectorList(hostList,hostsPerSector,numFailureZones);
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}
	
	
	protected void start(String experimentName, String outputFolder, int HostsPerSector,int numOfFailureZones) {
		System.out.println("Starting " + experimentName);
		
		VmAllocationPolicy vmAllocationPolicy = new SectorAllocationPolicyHoneyBeeV2(hostList, sectorList,vmList);
	
		try {
		ReplicationEnhancedPowerDatacenter datacenter = (ReplicationEnhancedPowerDatacenter) EnhancedHelper.createReplicationEnhancedDatacenter(
					"Datacenter",
					ReplicationEnhancedPowerDatacenter.class,
					hostList,
					vmAllocationPolicy,
					HostsPerSector,
					numOfFailureZones,
					vmList);  
			datacenter.setDisableMigrations(false);
			
			/*EnhancedPowerDatacenter datacenter = (EnhancedPowerDatacenter) EnhancedHelper.createEnhancedDatacenter(
					"Datacenter",
					EnhancedPowerDatacenter.class,
					hostList,
					vmAllocationPolicy,
				HostsPerSector,
				numOfFailureZones,
				vmList);  
			datacenter.setDisableMigrations(false);*/

			broker.submitVmList(vmList);
			broker.submitCloudletList(cloudletList);


			CloudSim.terminateSimulation(10*Constants.SIMULATION_LIMIT);

			double lastClock = CloudSim.startSimulation();

			List<Cloudlet> newList = broker.getCloudletReceivedList();
			Log.printLine("Received " + newList.size() + " cloudlets"); 

			CloudSim.stopSimulation();

			Helper.printResults(
					datacenter,
					vmList,
					lastClock,
					experimentName,
					Constants.OUTPUT_CSV,
					outputFolder);

		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}

		Log.printLine("Finished " + experimentName);
	}

	
	
	
	/**
	 * Gets the experiment name.
	 * 
	 * @param args the args
	 * @return the experiment name
	 */
	protected String getExperimentName(String... args) {
		StringBuilder experimentName = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			if (args[i].isEmpty()) {
				continue;
			}
			if (i != 0) {
				experimentName.append("_");
			}
			experimentName.append(args[i]);
		}
		return experimentName.toString();
	}

	

	
	
}